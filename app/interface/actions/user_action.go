package actions

import (
	"encoding/json"
	"errors"
	"gitlab.com/gojobpr/logger"
	"gitlab.com/gojobpr/persistance"
	"jobproject/models"
	"net/http"

	"github.com/jackc/pgconn"
)

type RegisterUser struct {
	baseAction
}

func (a RegisterUser) Handle(w http.ResponseWriter, r *http.Request) {
	body, err := a.readBody(w, r)
	if err == nil {
		return
	}

	user, err := models.CreateFromJson(body)
	if err != nil {
		handleErrorResponse(w, err, "something went wrong", http.StatusInternalServerError)
		return
	}

	record := persistance.PG.Create(user)
	if record.Error != nil {
		var pqErr *pgconn.PgError
		ok := errors.As(record.Error, &pqErr)
		if ok && pqErr.Code == "23505" {
			handleErrorResponse(w, err, "user already exists", http.StatusConflict)
			return

		}

		handleErrorResponse(w, err, "something went wrong", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
}

func handleErrorResponse(w http.ResponseWriter, err error, m string, s int) {
	if len(m) < 1 {
		m = "Invalid body provided"
	}
	logger.Log.Error().Err(err).Msg(m)
	jsonBody, _ := json.Marshal(map[string]string{
		"error": m,
	})

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(s)
	w.Write(jsonBody)
}
