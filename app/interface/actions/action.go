package actions

import (
	"encoding/json"
	"gitlab.com/gojobpr/logger"
	"io"
	"net/http"
)

type baseAction struct {
}

func (action baseAction) readBody(w http.ResponseWriter, r *http.Request) ([]byte, error) {
	body, err := io.ReadAll(r.Body)
	if err != nil {
		logger.Log.Error().Err(err).Msg("Invalid body provided")
		jsonBody, _ := json.Marshal(map[string]string{
			"error": "Invalid body provided",
		})

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusInternalServerError)
		w.Write(jsonBody)
		return nil, err
	}

	return body, nil
}
