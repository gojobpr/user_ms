package main

import (
	"errors"
	"fmt"
	"jobproject/models"
	"net/http"
	"os"

	_ "github.com/joho/godotenv/autoload"

	"gitlab.com/gojobpr/logger"
	"gitlab.com/gojobpr/persistance"
)

func main() {
	f := logger.ConfigureInit(logger.NewConfig(os.Getenv("LOG_SOURCE"), fmt.Sprintf("logs.log_%v.log", os.Getenv("ENV"))))
	if f != nil {
		defer f.Close()
	}
	persistance.PostgresConnect(persistance.PostgresConfig{DSN: os.Getenv("DATABASE_DSN")})
	persistance.PG.AutoMigrate(models.User{})

	server := &http.Server{
		Addr:    ":3333",
		Handler: AddRoutes(),
	}

	err := server.ListenAndServe()
	if errors.Is(err, http.ErrServerClosed) {
		fmt.Printf("server closed\n")
	} else if err != nil {
		fmt.Printf("error listening for server: %s\n", err)
	}
}
