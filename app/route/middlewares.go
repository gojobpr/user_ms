package route

import (
	"context"
	"encoding/json"
	"fmt"
	"gitlab.com/gojobpr/logger"
	"net/http"
	"time"

	"github.com/justinas/alice"
	"github.com/throttled/throttled/v2"
	"github.com/throttled/throttled/v2/store/memstore"
)

type Middlewares struct {
	alice.Chain
}

func NewMiddlewares(constructors ...alice.Constructor) Middlewares {
	return Middlewares{alice.New(constructors...)}
}

func DefaultMiddlewaresStack() Middlewares {
	return NewMiddlewares(
		RequestLogHandler,
		PanicRecoveryHandler,
		ThrottleHandler,
		TimeoutHandler,
	)
}

// add request to the log "SetRequest" and immediatly log each incoming request/response
func RequestLogHandler(h http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		logger.SetRequest(r)
		logger.Log.Info().Msg("REQUEST RECEIVED")
		startTime := time.Now()
		duration := time.Since(startTime)
		h.ServeHTTP(w, r)
		logger.Log.Info().Stringer("duration", duration).Msg("RESPONSE SENT")

	}
	return http.HandlerFunc(fn)
}

func AuthHandler(next http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		authToken := r.Header.Get("Authorization")
		var (
			user string
			err  error
		)

		user = authToken

		if err != nil {
			http.Error(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
			return
		}

		r = r.WithContext(context.WithValue(r.Context(), "authUser", user))

		next.ServeHTTP(w, r)
	}

	return http.HandlerFunc(fn)
}

func PanicRecoveryHandler(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			if recErr := recover(); recErr != nil {
				logger.Log.Error().Err(fmt.Errorf("%v", recErr)).Msg("Recovered from error")

				jsonBody, _ := json.Marshal(map[string]string{
					"error": "There was an internal server error",
				})

				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusInternalServerError)
				w.Write(jsonBody)
			}

		}()

		h.ServeHTTP(w, r)

	})
}

func ThrottleHandler(h http.Handler) http.Handler {
	store, err := memstore.New(65536)
	if err != nil {
		panic(err.Error())
	}

	quota := throttled.RateQuota{
		MaxRate:  throttled.PerMin(20),
		MaxBurst: 5,
	}
	rateLimiter, err := throttled.NewGCRARateLimiter(store, quota)
	if err != nil {
		panic(err.Error())
	}

	httpRateLimiter := throttled.HTTPRateLimiter{
		RateLimiter: rateLimiter,
		VaryBy:      &throttled.VaryBy{Path: true},
	}

	return httpRateLimiter.RateLimit(h)
}

func TimeoutHandler(h http.Handler) http.Handler {
	return http.TimeoutHandler(h, 1*time.Second, "timed out")
}
