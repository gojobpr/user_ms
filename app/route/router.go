package route

import (
	"net/http"

	"github.com/julienschmidt/httprouter"
)

type Router struct {
	*httprouter.Router
}

func NewRouter() *Router {
	return &Router{httprouter.New()}
}

func (r *Router) Get(path string, middlewares Middlewares, fc func(http.ResponseWriter, *http.Request)) {
	r.GET(path, wrapHandler(middlewares.ThenFunc(fc)))
}

func (r *Router) Post(path string, middlewares Middlewares, action func(http.ResponseWriter, *http.Request)) {
	r.POST(path, wrapHandler(middlewares.ThenFunc(action)))
}

func (r *Router) Put(path string, middlewares Middlewares, action func(http.ResponseWriter, *http.Request)) {
	r.PUT(path, wrapHandler(middlewares.ThenFunc(action)))
}

func (r *Router) Delete(path string, middlewares Middlewares, action func(http.ResponseWriter, *http.Request)) {
	r.DELETE(path, wrapHandler(middlewares.ThenFunc(action)))
}

func (r *Router) Head(path string, middlewares Middlewares, action func(http.ResponseWriter, *http.Request)) {
	r.HEAD(path, wrapHandler(middlewares.ThenFunc(action)))
}

func (r *Router) Options(path string, middlewares Middlewares, action func(http.ResponseWriter, *http.Request)) {
	r.OPTIONS(path, wrapHandler(middlewares.ThenFunc(action)))
}

func (r *Router) Patch(path string, middlewares Middlewares, action func(http.ResponseWriter, *http.Request)) {
	r.PATCH(path, wrapHandler(middlewares.ThenFunc(action)))
}

func wrapHandler(h http.Handler) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		h.ServeHTTP(w, r)
	}
}
