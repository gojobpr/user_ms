package utils

import "golang.org/x/crypto/bcrypt"

func Hash(s string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(s), 10)
	if err != nil {
		return "", err
	}
	return string(bytes), nil
}

func IsSameHash(s string, h string) error {
	return bcrypt.CompareHashAndPassword([]byte(h), []byte(s))
}
