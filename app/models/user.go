package models

import (
	"encoding/json"
	"jobproject/utils"

	"gorm.io/gorm"
)

type User struct {
	gorm.Model
	FirstName string `json:"fir,omitempty" gorm:"not null;default:null"`
	LastName  string `json:"name,omitempty" gorm:"not null;default:null"`
	Username  string `json:"username,omitempty" gorm:"unique;not null;default:null"`
	Email     string `json:"email,omitempty" gorm:"unique;not null;default:null"`
	Password  string `json:"password,omitempty" gorm:"not null;default:null"`
}

func CreateFromJson(body []byte) (*User, error) {
	user := &User{}
	err := json.Unmarshal(body, &user)
	if err != nil {
		return nil, err
	}
	hash, err := utils.Hash(user.Password)
	if err != nil {
		return nil, err
	}

	user.Password = hash

	return user, nil
}
