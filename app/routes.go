package main

import (
	"jobproject/interface/actions"
	"jobproject/route"
)

func AddRoutes() *route.Router {
	defaultMiddlewaresStack := route.DefaultMiddlewaresStack()

	router := route.NewRouter()

	router.Post("/register", defaultMiddlewaresStack, actions.RegisterUser{}.Handle)

	return router
}
